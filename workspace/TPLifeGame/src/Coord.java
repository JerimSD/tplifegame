
public class Coord {
		int lig, col;
		private int x, y;

	    public Coord(int x, int y) {
	        this.x = x;
	        this.y = y;
	    }
	    
	    @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (!(o instanceof Coord)) return false;
	        Coord paire = (Coord) o;
	        return x == paire.x && y == paire.y;
	    }

	    @Override
	    public int hashCode() {
	        String h = String.format("%d %d", x, y);
	        return h.hashCode();
	    }	
	
}