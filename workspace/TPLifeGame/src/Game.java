import java.util.HashSet;


public class Game {
	private HashSet<Coord> damier;
    private int nbLigs, nbCols;

    public Game(Coord[] listeInitiale, int nbLigs, int nbCols){
		this.damier = new HashSet<Coord>();
		this.nbLigs = nbLigs;
		this.nbCols = nbCols;
	}
    
    public Game(int nbLigs, int nbCols) {
		this.nbLigs = nbLigs;
		this.nbCols = nbCols;
	}
	
    public boolean estVivante(Coord coord) {
		if(damier.contains(coord)) return true;
		else return false;		
	}
    
    private String showCell(Coord coord) {
    	if(damier.contains(coord)) return "#";
    	else return ".";
    }
    
    private String showLig(int numLig){
    	String res="";
    	for(int i=0;i<nbCols;i++){
    		Coord coords = new Coord(numLig,i);
    		res = res + showCell(coords);
    	}
    	return res;
    }
    
    @Override
    public String toString(){
    	String res="";
    	for(int i=0;i<nbLigs;i++){
    		res = res + showLig(i) + "\n";
    	}
    	return res ;
    }
    
    
    // MAIN
	public static void main(String[] args) {
		Coord[] coords = {	new Coord(0, 3), new Coord(1, 2),
							new Coord(2, 2), new Coord(2, 3),
							new Coord(2, 4)};
		Game jeu = new Game(coords, 5, 5);
	
		System.out.println(jeu);
	}
	//END MAIN
	
	
	
}
